const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const app = express();
const env = process.env();

app.use(express.json());
app.use('/users', require("routes/users.js"));

app.listen(env.PORT, () => {
	console.log(`Server is connected at port ${env.PORT}`);
});
mongoose.connect(env.DB_CONNECTION_STRING);
mongoose.connection.once('open', () => {
	console.log(`Server is now connected to the database`);
});

app.get('/', (req, res) => {
	res.send("Welcome to Enrico Miguel H. Silvano's App");
});